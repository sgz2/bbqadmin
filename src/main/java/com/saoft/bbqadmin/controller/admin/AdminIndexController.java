package com.saoft.bbqadmin.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("admin")
public class AdminIndexController {

    @RequestMapping(value = {"/","","index"},method = RequestMethod.GET)
    public String index(){
        return "admin/index";
    }

    @RequestMapping(value = "welcome",method = RequestMethod.GET)
    public String welcome(){
        return "admin/welcome";
    }
}
