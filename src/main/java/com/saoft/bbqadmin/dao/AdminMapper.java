package com.saoft.bbqadmin.dao;

import com.saoft.bbqadmin.entity.Admin;
import com.saoft.bbqadmin.entity.AdminExample;
import java.util.List;

public interface AdminMapper {
    int countByExample(AdminExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Admin record);

    int insertSelective(Admin record);

    List<Admin> selectByExample(AdminExample example);

    Admin selectByPrimaryKey(Integer id);
}