package com.saoft.bbqadmin.common.web;

public class Seo {

    /**
     * 网站名称title
     */
    private String mainName;

    public String getMainName() {
        return mainName;
    }

    public void setMainName(String mainName) {
        this.mainName = mainName;
    }
}
