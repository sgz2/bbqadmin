/**
 * 管理初始化
 */
var User = {
    id: "UserTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
User.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '登录名', field: 'username', visible: true, align: 'center', valign: 'middle'},
        {title: '密码', field: 'password', visible: true, align: 'center', valign: 'middle'},
        {title: '姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle'},
        {title: '头像', field: 'headImg', visible: true, align: 'center', valign: 'middle'},
        {title: '简介', field: 'intro', visible: true, align: 'center', valign: 'middle'},
        {title: '通信地址', field: 'address', visible: true, align: 'center', valign: 'middle'},
        {title: '电话', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
        {title: '注册时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {title: '最后更新时间', field: 'updateTime', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
User.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        BBQ.info("请先选中表格中的某一记录！");
        return false;
    }else{
        User.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
User.openAddUser = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: BBQ.ctxPath + '/admin/user/user_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
User.openUserDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: BBQ.ctxPath + '/admin/user/user_edit?id=' + User.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
User.delete = function () {
    if (this.check()) {
        $.post(BBQ.ctxPath + "/admin/user/delete",{id:this.seItem.id}, function (data) {
            BBQ.success("删除成功!");
            User.table.refresh();
        });
    }
};

/**
 * 查询列表
 */
User.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    User.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = User.initColumn();
    var table = new BSTable(User.id, "/admin/user/list", defaultColunms);
    table.setPaginationType("server");
    User.table = table.init();
});
