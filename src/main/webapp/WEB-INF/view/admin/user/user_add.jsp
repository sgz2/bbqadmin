<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>用户新增 - ${seo.mainName}</title>
    <jsp:include page="../../common/link.jsp"/>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <div class="form-horizontal">

            <div class="row">
                <div class="col-sm-6 b-r">
                    <form:input id="id" name="" underline="true"/>
                    <form:input id="username" name="登录名" underline="true"/>
                    <form:input id="password" name="密码" underline="true"/>
                    <form:input id="name" name="姓名" underline="true"/>
                    <form:input id="sex" name="性别" underline="true"/>
                    <form:input id="headImg" name="头像" />
                </div>

                <div class="col-sm-6">
                    <form:input id="intro" name="简介" underline="true"/>
                    <form:input id="address" name="通信地址" underline="true"/>
                    <form:input id="phone" name="电话" underline="true"/>
                    <form:input id="email" name="邮箱" underline="true"/>
                    <form:input id="createTime" name="注册时间" underline="true"/>
                    <form:input id="updateTime" name="最后更新时间" />
                </div>
            </div>

            <div class="row btn-group-m-t">
                <div class="col-sm-10">
                    <form:button btnCss="info" name="提交" id="ensure" icon="fa-check" clickFun="UserInfoDlg.addSubmit()"/>
                    <form:button btnCss="danger" name="取消" id="cancel" icon="fa-eraser" clickFun="UserInfoDlg.close()"/>
                </div>
            </div>
        </div>

    </div>
</div>
<jsp:include page="../../common/script.jsp"/>
<script src="/static/app/js/admin/user/user_info.js?v=0.123"></script>
</body>
</html>