<%@ taglib tagdir="/WEB-INF/tags" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>社团管理系统 - 社团管理</title>
    <jsp:include page="../../common/link.jsp"/>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <div class="form-horizontal">

            <div class="row">
                <div class="col-sm-6 b-r">
                                        <form:input id="id" name="" value="${item.id}" underline="true"/>
                    <form:input id="username" name="登录名" value="${item.username}" underline="true"/>
                    <form:input id="password" name="密码" value="${item.password}" underline="true"/>
                    <form:input id="name" name="姓名" value="${item.name}" underline="true"/>
                    <form:input id="sex" name="性别" value="${item.sex}" underline="true"/>
                    <form:input id="headImg" name="头像" value="${item.headImg}" />
                </div>

                <div class="col-sm-6">
                    <form:input id="intro" name="简介" value="${item.intro}" underline="true"/>
                    <form:input id="address" name="通信地址" value="${item.address}" underline="true"/>
                    <form:input id="phone" name="电话" value="${item.phone}" underline="true"/>
                    <form:input id="email" name="邮箱" value="${item.email}" underline="true"/>
                    <form:input id="createTime" name="注册时间" value="${item.createTime}" underline="true"/>
                    <form:input id="updateTime" name="最后更新时间" value="${item.updateTime}" />
                </div>
            </div>

            <div class="row btn-group-m-t">
                <div class="col-sm-10">
                    <form:button btnCss="info" name="提交" id="ensure" icon="fa-check" clickFun="UserInfoDlg.editSubmit()"/>
                    <form:button btnCss="danger" name="取消" id="cancel" icon="fa-eraser" clickFun="UserInfoDlg.close()"/>
                </div>
            </div>
        </div>

    </div>
</div>
<jsp:include page="../../common/script.jsp"/>
<script src="/static/app/js/admin/user/user_info.js?v=0.123"></script>
</body>
</html>