<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="/static/plugins/jquery/dist/jquery.min.js"></script>
<script src="/static/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/static/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/plugins/fastclick/lib/fastclick.js"></script>
<script src="/static/plugins/adminlte/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/static/plugins/bootstrap-table/dist/bootstrap-table.min.js"></script>
<script src="/static/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="/static/plugins/layer/layer.js"></script>
<script src="/static/plugins/validate/bootstrapValidator.min.js"></script>
<script src="/static/plugins/validate/zh_CN.js"></script>
<script src="/static/app/js/bootstrap-table-object.js?v=0.12"></script>
<script src="/static/app/js/BBQ.js"></script>
<script src="/static/app/js/contabs.js"></script>

