# bbqadmin后台管理系统

#### 介绍
基于ssm、mysql；包含excel导入导出、富文本、分页、echarts等基础功能的后台管理系统，主要用于学习简单基本的技术。

#### 技术点
1. 图片/文件上传
2. excel文件导入导出
3. 富文本编辑器
4. echarts可视化图表

#### 软件架构

1. jsp做为视图
2. 后台采用springmvc spring mybatis
3. 前端页面bootstrap,bootstrapTable,bootstrapValidate,jquery
4. 数据库使用mysql

#### 安装教程

1. mysql5 (不支持mysql8)
2. jdk1.8
3. maven
4. 开发工具idea，（完成后兼容eclipse）

#### 功能说明

##### 管理员
1. 个人资料、密码修改、登录 
2. 用户管理、初始化用户密码、修改资料、导入导出用户信息 （可视化）
3. 新闻类型、新闻管理（富文本编辑器）
4. 申请类型管理、审批申请

##### 普通用户
1. 个人资料、密码修改、登录、注册
2. 查看新闻（分页）
3. 提交申请、查看申请进度


#### 引用

1. Guns js部分

